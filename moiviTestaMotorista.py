import pandas as pd
import moiviBDConecta as con

#global variables
queryProfile = 'select driver_profile_id, count(*) as viagens from trips group by driver_profile_id'
conn = con.connect()
colunas = ['driver_profile_id', 'latitude', 'longitude', 'altitude', 'perc', 'hora']


def buscaquery():
    querytrip = 'select * from ('
    querytrip = querytrip + "select tp.driver_profile_id, p.latitude, p.longitude, p.altitude, tp.id, date_part('hour',p.created_at) as hora, "
    querytrip = querytrip + ' rank() over (partition by tp.id order by p.id)  as rank '
    querytrip = querytrip + 'from trips tp inner join positions p on tp.id = p.trip_id '
    querytrip = querytrip + ' where discarted = 0 ) as ranked'
    querytrip = querytrip + ' where rank < 10'

    return querytrip


def criasaida():
    dfsaida = pd.DataFrame(columns=colunas)
    return dfsaida


def inseresaida(dfsaida, valores):
    dfsaida = dfsaida.append(pd.DataFrame([valores], columns=colunas))
    return dfsaida


def carregaDFProfile():
    df = con.execquery(conn, queryProfile)
    return df


def buscatodasviagens():
    dfTrips = con.execquery(conn, queryprincipal)
    return dfTrips


def processaProfile():
    dfProfile = carregaDFProfile()
    dfsaida = criasaida()
    dfalltrips = buscatodasviagens()

    for i in range(len(dfProfile)):
        if dfProfile.viagens[i] > 10:
            dfTrips = buscaviagens(dfProfile.driver_profile_id[i], dfalltrips)
            dfsaida = viagemMotorista(dfTrips, dfsaida)
            print(dfsaida)

    return dfsaida

def buscaviagens(prof, dfviagens):
    dfRet = dfviagens[dfviagens['driver_profile_id'] == prof]
    dfRet = dfRet.reset_index()
    return dfRet

def viagemMotorista(dfTrips, dfsaida):
    latitudes = dfTrips.latitude
    latitudes = list(dict.fromkeys(latitudes))

    for latitude in latitudes:
        df = dfTrips[dfTrips.latitude.isin([latitude])]
        df = df.reset_index()
        longitudes = df.longitude
        longitudes = list(dict.fromkeys(longitudes))
        for longitude in longitudes:
            df2 = df[df.longitude.isin([longitude])]
            df2 = df2.reset_index(drop=True)
            if len(df2) > 29:
                perc = 95
            else:
                qt = len(df2)
                perc = (100 * qt) / 30

            if perc > 10:
               values = [df2.driver_profile_id[0], df2.latitude[0], df2.longitude[0], df2.altitude[0], perc, df2.hora[0]]
               dfsaida = inseresaida(dfsaida, values)

    return dfsaida


def gravasaida(dfsaida):
    eng = con.engine()
    eng.execute('DROP TABLE driver_possible_trips;')
    dfsaida.to_sql('driver_possible_trips', eng, if_exists='append')


queryprincipal = buscaquery()
dfsaida = processaProfile()
export = dfsaida.to_json(r'/home/darlan/moivi/inicioviagem.json', orient='records')
gravasaida(dfsaida)




