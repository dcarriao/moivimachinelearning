import pandas as pd
import moiviBDConecta as con
import numpy as np

#global variables
querytrips = 'select distinct trip_id from positions where length(data) > 120 '
connec = con.connect()
colunas = ['trip_id', 'aceleracoes', 'frenagens', 'vel media', 'distancia']


def criasaida():
    dfsaida = pd.DataFrame(columns=colunas)
    return dfsaida


def inseresaida(dfsaida, valores):
    dfsaida = dfsaida.append(pd.DataFrame([valores], columns=colunas))
    return dfsaida


def selecionaviagens():
    dfTrips = con.execquery(connec, querytrips)
    return dfTrips


def buscaviagem(trip):
    querytrip = 'select data, created_at from positions where trip_id =' + str(trip) + ' order by created_at '
    dftrip = con.execquery(connec, querytrip)
    return dftrip


def cleandata(df):
    df.data = df.data.str.replace('"valid":', '')
    df.data = df.data.str.replace('true,', '')
    df.data = df.data.str.replace('false,', '')
    df.data = df.data.str.replace(':', ' ')
    df.data = df.data.str.replace('}', '')
    df.data = df.data.str.replace('{', '')
    df.data = df.data.str.replace('"', '')
    df.data = df.data.str.replace(',', ' ')

    return df


def gravasaida(dfsaida):
    eng = con.engine()
    eng.execute('DROP TABLE trip_acelerometro_data;')
    dfsaida.to_sql('trip_acelerometro_data', eng, if_exists='append')


def rodaavaliacao():
    dftrips = selecionaviagens()
    trips = dftrips.trip_id
    dfsaida = criasaida()
    for trip in trips:
        dftrip = buscaviagem(trip)
        dftrip = cleandata(dftrip)
        sp1 = dftrip.data.str.split()
        tempo = calculatempo(dftrip.created_at)
        v = calculavelocidade(sp1, tempo)
        dfsaida = calculafrenagens(trip, v, tempo, dfsaida)

    gravasaida(dfsaida)


def calculatempo(tempo):
    temp = []
    for i in range(len(tempo)):
        if i == 0:
            temp.insert(i, 0)
        else:
            sec = (tempo[i] - tempo[i - 1]) / np.timedelta64(1, 's')
            gra = sec

            if gra < 0:
                gra = 0

            if gra > 50:
                gra = 0

            temp.insert(i, gra)
    return temp


def calculavelocidade(sp1, temp):
    dt = temp
    v = []
    ant = 0
    alpha = 0.5
    vxa = 0
    vya = 0
    vza = 0
    dta = 1
    for i in range(len(sp1)):

        if dt[i] > 0:

            acy = float(sp1[i][7]) * 9.81
            acx = float(sp1[i][9]) * 9.81
            acz = float(sp1[i][11])

            if abs(round(acy)) == 0 and abs(round(acx)) == 0:
                acz = 0

            acx = acx * alpha + (acx * (1.0 - alpha))
            acy = acy * alpha + (acy * (1.0 - alpha))
            acz = acz * alpha + (acz * (1.0 - alpha))

            vx = vxa + acx * dta
            vy = vya + acy * dta
            vz = vza + acz * dta

            vxa = (vx - vxa)
            vya = (vy - vya)
            vza = (vz - vza)
            dta = dt[i]

            av = np.sqrt(vy ** 2 + vz ** 2 + vx ** 2)

            a = np.sqrt(acy ** 2 + acx ** 2 + acz ** 2)

            da = (a - 9.81) * 3.6 * 0.5

            vel = ((a * 3.6) * 0.5) / dt[i]

            v.insert(i, av)

            # print('velocidade ponto ', i, ': ', A, ' m/s', round(abs(V[i])), ' km/h', ' tempo:', dt[i], 'da:', da)

            #print('vx:', vx * 3.6 * 0.5, ' vy:', vy * 3.6 * 0.5, ' vz: ', vz * 3.6 * 0.5, 'Av:', Av * 3.6 * 0.5)

            ant = v[i]

        else:

            v.insert(i, ant)
    return v


def calculafrenagens(trip, v, dt, dfsaida):
    ant = 0
    acel = 0
    frei = 0
    vel = 0
    tmp = 0
    dist = 0
    disttot = 0
    t = 1
    tempo = 0
    for i in range(len(v)):
        if i > 0:
            # print('vel atual:', V[i], ' vel anterior:', ant, 'distancia:', dist, ' tempo:', tempo, ' sec.', dt[i])
            if (v[i] - ant) > 11:
                if dt[i] == 1 and i > 1:
                    acel = acel + 1

            if (ant - v[i]) > 11:
                if dt[i] == 1:
                    frei = frei + 1

            if abs(round(v[i])) > 0:
                tempo = tempo + dt[i]
            vel = vel + v[i]
            ant = v[i]


        else:
            ant = v[i]
            tempo = tempo + dt[i]

    media = round(vel / len(v), 2)
    tmpm = (tempo / 3600)
    ms = (vel / 3.6) / tempo
    dist = round((vel * tmpm), 3)
    m = dist * tmpm
    tmpm = (tempo / 60)

    print('Aceleracoes:', acel, ' frenagens:', frei, 'Vel. Media:', media, 'Distancia (M):', dist, 'tempo(M):', tmpm,
          ' m/s:', ms)

    values = [trip, acel, frei, media, dist]
    dfsaida = inseresaida(dfsaida, values)

    return dfsaida


rodaavaliacao()
